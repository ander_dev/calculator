package com.calculator.services.impl;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.calculator.services.CalculatorService;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    private static final String NOTATION_REGEX = "[0-9+\\-/*\\(\\)]+";

    @Override
    public String evaluateExpression(String value) {
        if(isCharacterValid(value)){
            DoubleEvaluator eval = new DoubleEvaluator();

            int result = eval.evaluate(value).intValue();
            return Integer.toString(result);
        } else {
            return "OPERATION NOT ALLOWED!";
        }

    }

    @Override
    public Boolean isCharacterValid(String value)  {

        Pattern pattern = Pattern.compile(NOTATION_REGEX);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}
