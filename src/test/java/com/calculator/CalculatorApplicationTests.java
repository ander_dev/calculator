package com.calculator;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.calculator.services.CalculatorService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"command.line.runner.enabled=false"})
public class CalculatorApplicationTests {

    @Autowired
    private CalculatorService calculatorService;


    @Test
    void testInvalidCharacterAlphaLowerCase() {
        Boolean ret = calculatorService.isCharacterValid("a");
        assertFalse(ret);
    }

    @Test
    void testInvalidCharacterAlphaUpperCase() {
        Boolean ret = calculatorService.isCharacterValid("S");
        assertFalse(ret);
    }

    @Test
    void testInvalidCharacterWrongCharacter() {
        Boolean ret = calculatorService.isCharacterValid("%");
        assertFalse(ret);
    }

    @Test
    void testValidCharacterPlus() {
        Boolean ret = calculatorService.isCharacterValid("+");
        assertTrue(ret);
    }

    @Test
    void testValidCharacterMinus() {
        Boolean ret = calculatorService.isCharacterValid("-");
        assertTrue(ret);
    }

    @Test
    void testValidCharacterMultiplication() {
        Boolean ret = calculatorService.isCharacterValid("*");
        assertTrue(ret);
    }

    @Test
    void testValidCharacterDivision() {
        Boolean ret = calculatorService.isCharacterValid("/");
        assertTrue(ret);
    }

    @Test
    void testValidCharacterOpenParenthesis() {
        Boolean ret = calculatorService.isCharacterValid("(");
        assertTrue(ret);
    }

    @Test
    void testValidCharacterCloseParenthesis() {
        Boolean ret = calculatorService.isCharacterValid(")");
        assertTrue(ret);
    }

    @Test
    void testValidOperation() {
        Boolean ret = calculatorService.isCharacterValid("(4+5)*2-6/2");
        assertTrue(ret);
    }

    @Test
    void testFailureEvaluation() {
        String result = calculatorService.evaluateExpression("sin((4+5)*(6-2)/2)");
        assertEquals("OPERATION NOT ALLOWED!", result);
    }

    @Test
    void testSuccessfulEvaluation() {
        String result = calculatorService.evaluateExpression("(4+5)*(6-2)/2");
        assertEquals("18", result);
    }

    @Test
    void testJavaExpressionEvaluator(){
        String expression = "(2+3)*(6-4)/3";
        DoubleEvaluator eval = new DoubleEvaluator();

        int result = eval.evaluate(expression).intValue();

        assertEquals(3, result);
    }
}
