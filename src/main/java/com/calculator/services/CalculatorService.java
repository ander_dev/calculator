package com.calculator.services;

public interface CalculatorService {

    String evaluateExpression(String value);

    Boolean isCharacterValid(String value);
}
