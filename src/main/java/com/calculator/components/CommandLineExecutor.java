package com.calculator.components;

import com.calculator.services.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@ConditionalOnProperty(
        prefix = "command.line.runner",
        value = "enabled",
        havingValue = "true",
        matchIfMissing = true)
public class CommandLineExecutor implements CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(CommandLineExecutor.class);

    private final CalculatorService calculatorService;

    public CommandLineExecutor(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @Override
    public void run(String... args) {
        LOG.debug("ENTERING : command line runner");

        System.out.println("Enter command \"quit\" to exit:");
        Scanner scan = new Scanner(System.in);
        boolean exit = false;
        while (!exit) {

            String value = scan.next();

            if(value != null) {
                if ("quit".equals(value)) {
                    System.out.println("Closing application");
                    exit = true;
                } else {
                    String ret = calculatorService.evaluateExpression(value);
                    System.out.println(ret);
                }
            }
        }
        scan.close();
    }
}
